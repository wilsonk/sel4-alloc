// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! Hierarchical virtual address space handling.
//!
//! This module assumes a particular flavor of paging structures. The paging structures are
//! hierarchical, with each level being able to store pages that are some power-of-two size. All
//! page sizes on level N are strictly greater than all page sizes on level N-1. An entry at level
//! N can store either a page of any size from level N, or a table whose entries are at level N-1.
//!
//! For example, x86 with PAE could be described as:
//!
//!  - Level 2: 4KiB, "PTE"
//!  - Level 1: 2MiB, "PDE"
//!  - Level 0: (no pages, only tables) "PDPT"
//!
//! ARMv7 could be described as:
//!
//!  - Level 2: 4KiB, 64KiB (pages)
//!  - Level 1: 1MiB, 16MiB (sections)
//!  - Level 0: (no pages, only tables)

use {CSpaceManager, UTSpaceManager};
use sel4_sys::*;
use core::mem::{size_of, align_of, transmute};
use core::cell::{RefCell, Cell};
use intrusive_collections::{rbtree, RBTree, Bound, IntrusiveRef};
use seL4_ARCH_VMAttributes;
use collections::Vec;
use alloc::boxed::Box;

// HIER_LEVELS is (objtype, level_sizes, bits_translated, size_bits)

#[cfg(all(target_arch = "arm", target_pointer_width = "32"))]
mod arch {
    use sel4_sys::*;
    use super::{MapFn, UnmapFn};
    pub type TblMapFn = unsafe fn(seL4_CPtr, seL4_CPtr, u32, seL4_ARM_VMAttributes) -> isize;
    pub const HIER_LEVELS: [(isize, [u8; 2], u8, u8); 2] = [(seL4_ARM_PageDirectoryObject as isize,
                                                             [12, 18],
                                                             8,
                                                             14),
                                                            (seL4_ARM_PageTableObject as isize,
                                                             [20, 24],
                                                             12,
                                                             10)];
    pub const MAP_PAGE: super::MapFn = seL4_ARM_Page_Map;
    pub const UNMAP_PAGE: UnmapFn = seL4_ARM_Page_Unmap;
    pub const MAP_FNS: [Option<(TblMapFn, UnmapFn)>; 2] =
        [Some((seL4_ARM_PageTable_Map as TblMapFn, seL4_ARM_PageTable_Unmap as UnmapFn)), None];
}

#[cfg(all(target_arch = "x86", target_pointer_width = "32"))]
mod arch {
    use sel4_sys::*;
    use super::{MapFn, UnmapFn};
    // Note: no PAE
    pub type TblMapFn = unsafe fn(seL4_CPtr, seL4_CPtr, u32, seL4_IA32_VMAttributes) -> isize;
    pub const HIER_LEVELS: [(isize, [u8; 1], u8, u8); 2] = [(seL4_IA32_PageDirectoryObject as isize,
                                                             [12],
                                                             10,
                                                             12),
                                                            (seL4_IA32_PageTableObject as isize,
                                                             [22],
                                                             10,
                                                             12)];
    pub const MAP_PAGE: super::MapFn = seL4_IA32_Page_Map as MapFn;
    pub const UNMAP_PAGE: UnmapFn = seL4_IA32_Page_Unmap;
    pub const MAP_FNS: [Option<(TblMapFn, UnmapFn)>; 2] =
        [Some((seL4_IA32_PageTable_Map as TblMapFn, seL4_IA32_PageTable_Unmap as UnmapFn)), None];
}

pub type MapFn = unsafe fn(seL4_CPtr,
                           seL4_CPtr,
                           u32,
                           seL4_CapRights,
                           seL4_ARCH_VMAttributes)
                           -> isize;
pub type UnmapFn = unsafe fn(seL4_CPtr) -> isize;


use self::arch::*;

/// This is the header which tracks information about a level in the paging hierarchy.
pub struct LevelNode {
    /// Cap for this table, passed to MAP_FNS[self.depth].1
    table_cap: seL4_CPtr,
    /// Index into HIER_LEVELS and MAP_FNS.
    depth: u8,
    /// Number of entries in this table.
    log2_size: u8,
}

#[derive(Clone, Copy)]
enum LevelEntry {
    Table(*mut LevelNode),
    Page {
        cap: seL4_CPtr,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
    },
    Free,
}

#[inline(always)]
fn bits() -> usize {
    ::core::mem::size_of::<usize>() * 8
}

fn get_n_bits_at(word: usize, n: u8, start: u8) -> usize {
    debug_assert!(n <= bits() as u8);
    debug_assert!(start < bits() as u8);
    debug_assert!(bits() <= 64); // y'know, just in case.
    (word >> start as usize) & (!0 >> ((bits() as u8 - n) as usize % bits()))
}

/// Create a new, empty level for use at `depth` in the paging hierarchy.
fn new_level<'a, A: ::AllocatorBundle>(alloc: &'a A, depth: u8) -> &'a LevelNode {
    // FIXME: why are these unused, they look important.
    // let size_bits = HIER_LEVELS[depth as usize].3;
    let cptr = alloc.cspace().allocate_slot(alloc).unwrap();
    alloc.utspace()
        .allocate_raw(alloc.cspace().slot_window(cptr).unwrap(),
                      HIER_LEVELS[depth as usize].3 as usize,
                      HIER_LEVELS[depth as usize].0)
        .unwrap();
    let ptr = unsafe {
        ::alloc::heap::allocate(LevelNode::total_size(HIER_LEVELS[depth as usize].2),
                                align_of::<LevelNode>()) as *mut LevelNode
    };
    assert!(!ptr.is_null());
    let obj = unsafe { transmute::<_, &mut LevelNode>(ptr) };
    obj.table_cap = cptr;
    obj.depth = depth;
    obj.log2_size = HIER_LEVELS[depth as usize].2;
    let tb = obj.get_table_pointer();
    for i in 0..1 << obj.log2_size {
        unsafe { *tb.offset(i) = LevelEntry::Free }
    }
    unsafe { transmute::<&mut LevelNode, &'a LevelNode>(obj) }
}

impl LevelNode {
    /// Calculate the number of bytes used to store this level, including the header, alignment,
    /// and table after it.
    fn total_size(log2_size: u8) -> usize {
        let mut sz = 0;
        sz += size_of::<LevelNode>();
        sz += align_of::<LevelEntry>() - sz % align_of::<LevelEntry>();
        sz += (1 << log2_size as usize) * size_of::<LevelEntry>();
        sz
    }

    /// Get a pointer to the first entry in the table.
    fn get_table_pointer(&self) -> *mut LevelEntry {
        let mut ptr = self as *const _ as usize;
        ptr += size_of::<LevelNode>();
        ptr += align_of::<LevelEntry>() - ptr % align_of::<LevelEntry>();
        debug_assert!(ptr % align_of::<LevelEntry>() == 0);
        ptr as *mut LevelEntry
    }

    /// Given a virtual address and the number of bits already translated, return the index into
    /// this level of the paging hierarchy which should be used to continue lookup.
    fn get_level_index(&self, vaddr: usize, bits_translated: usize) -> usize {
        let n = HIER_LEVELS[self.depth as usize].2;
        get_n_bits_at(vaddr, n, bits() as u8 - bits_translated as u8 - n)
    }

    /// Walk the paging hierarchy, calling back for each level which influences the lookup of
    /// `vaddr`.
    ///
    /// If the callback returns `None`, walking continues. Otherwise, it returns the return value
    /// of the callback. If the entire table is traversed without the callback returning `Some`,
    /// `None` is returned.
    fn walk_table<T, F: FnMut(u8, *mut LevelEntry) -> Option<T>>(&self,
                                                                 vaddr: usize,
                                                                 mut f: F)
                                                                 -> Option<T> {
        let mut cur = unsafe { transmute::<_, &'static LevelNode>(self) };
        let mut bits_translated = 0;
        let mut depth = 0;
        loop {
            let tp = cur.get_table_pointer();
            let n = HIER_LEVELS[depth as usize].2;
            let tbidx = get_n_bits_at(vaddr, n, bits() as u8 - bits_translated as u8 - n);
            let ptr = unsafe { tp.offset(tbidx as isize) };
            match f(depth, ptr) {
                c @ Some(_) => return c,
                _ => (),
            }
            match unsafe { *ptr } {
                LevelEntry::Table(p) => {
                    cur = unsafe { transmute::<*mut LevelNode, &LevelNode>(p) };
                }
                _ => return None,
            }
            bits_translated += n;
            depth += 1;
        }
    }

    /// Walk the paging hierarchy, calling back for each `LevelEntry` in a given range of
    /// addresses.
    ///
    /// If the callback returns `None`, walking continues. Otherwise, it returns the return value
    /// of the callback. If the entire range is traversed without the callback returning `Some`,
    /// `None` is returned.
    fn walk_table_range<T, F: FnMut(u8, usize, *const LevelNode, *mut LevelEntry) -> Option<T>>
        (&self,
         start: usize,
         end: usize,
         mut f: F)
         -> Option<T> {
        self.walk_table_range_inner(start, end, 0, &mut f)
    }

    fn walk_table_range_inner<T,
                              F: FnMut(u8, usize, *const LevelNode, *mut LevelEntry) -> Option<T>>
        (&self,
         start: usize,
         end: usize,
         bits_translated: usize,
         f: &mut F)
         -> Option<T> {
        let cur = unsafe { transmute::<_, &'static LevelNode>(self) };
        let start_lvidx = self.get_level_index(start, bits_translated);
        let end_lvidx = self.get_level_index(end, bits_translated);
        let tp = cur.get_table_pointer();
        for i in start_lvidx..end_lvidx {
            let ptr = unsafe { tp.offset(i as isize) };
            match f(cur.depth,
                    start +
                    (1 << (HIER_LEVELS[cur.depth as usize].2 as usize)) * (i - start_lvidx),
                    cur,
                    ptr) {
                c @ Some(_) => return c,
                _ => (),
            }
            match unsafe { *ptr } {
                LevelEntry::Table(p) => {
                    let p = unsafe { transmute::<_, &'static LevelNode>(p) };
                    match p.walk_table_range_inner(start,
                                                   end,
                                                   bits_translated +
                                                   (HIER_LEVELS[cur.depth as usize].2 as usize),
                                                   f) {
                        c @ Some(_) => return c,
                        _ => (),
                    }
                }
                _ => {}
            }
        }
        None
    }

    fn map_pages_at_vaddr<A: ::AllocatorBundle>(&self,
                                    caps: &[seL4_CPtr],
                                    vaddr: usize,
                                    bits_translated: usize,
                                    rights: seL4_CapRights,
                                    size_bits: u8,
                                    attrs: seL4_ARCH_VMAttributes,
                                    alloc: &A,
                                    mock: bool) {
        let tp = self.get_table_pointer();
        let tbidx = self.get_level_index(vaddr, bits_translated);
        let level_size = 1 << size_bits;
        if HIER_LEVELS[self.depth as usize].1.contains(&size_bits) {
            // map at this level.
            for (idx, &cap) in caps.iter().enumerate() {
                assert!(tbidx + idx < (1 << self.log2_size));
                match unsafe { *tp.offset((tbidx + idx) as isize) } {
                    LevelEntry::Free => {
                        unsafe {
                            *tp.offset((tbidx + idx) as isize) = LevelEntry::Page {
                                cap: cap,
                                rights: rights,
                                attrs: attrs,
                            }
                        };
                        assert!(MAP_FNS[self.depth as usize].is_some());
                        if !mock {
                            let res = unsafe {
                                MAP_PAGE(cap,
                                         self.table_cap,
                                         (vaddr + (idx * level_size)) as seL4_Word,
                                         rights,
                                         attrs)
                            };
                            assert!(res == 0);
                        }
                    } 
                    _ => {
                        panic!("page table occupied!");
                    }
                }
            }
        } else {
            // we must go deeper.

            assert!(self.depth + 1 < HIER_LEVELS.len() as u8);
            // might need to split the request across multiple table entries.
            let max_elts = 1 << HIER_LEVELS[self.depth as usize + 1].2;

            for (idx, cap) in caps.chunks(max_elts).enumerate() {
                assert!(tbidx + idx < (1 << self.log2_size));
                let tb;
                match unsafe { *tp.offset((tbidx + idx) as isize) } {
                    LevelEntry::Free => {
                        // Insert a table, and then continue.
                        tb = new_level(alloc, self.depth + 1);
                        unsafe {
                            *tp.offset((tbidx + idx) as isize) =
                                LevelEntry::Table(tb as *const _ as *mut LevelNode)
                        };
                    }
                    LevelEntry::Table(new_tb) => {
                        tb = unsafe { transmute::<*mut LevelNode, &LevelNode>(new_tb) };
                    }
                    LevelEntry::Page { .. } => panic!("page present where a table was wanted!"),
                }
                tb.map_pages_at_vaddr(cap,
                                      vaddr + (idx * level_size),
                                      bits_translated +
                                      (HIER_LEVELS[self.depth as usize + 1].2 as usize),
                                      rights,
                                      size_bits,
                                      attrs,
                                      alloc,
                                      mock);
            }
        }
    }

    fn change_protection<A: ::AllocatorBundle>(&self,
                                   vaddr: usize,
                                   bytes: usize,
                                   rights: seL4_CapRights,
                                   attrs: seL4_ARCH_VMAttributes,
                                   _: &A,
                                   mock: bool) {
        let bytes = bytes + bytes % 4096;
        self.walk_table_range(vaddr, vaddr + bytes, |_, addr, parent, level_node| {
            match unsafe { *level_node } {
                LevelEntry::Page { cap, .. } => unsafe {
                    if !mock {
                        UNMAP_PAGE(cap);
                        MAP_PAGE(cap, (*parent).table_cap, addr as seL4_Word, rights, attrs);
                    }
                },
                _ => {}
            }
            None::<()>
        });
    }

    fn get_level_entry(&self, vaddr: usize) -> Option<&LevelEntry> {
        self.walk_table(vaddr, |_, entry| {
            match unsafe { *entry } {
                LevelEntry::Page { .. } => Some(unsafe { transmute::<_, &LevelEntry>(entry) }),
                _ => None,
            }
        })
    }
}

pub struct ReservationNode {
    num_pages: Cell<usize>,
    start_addr: Cell<usize>,
    address_link: rbtree::Link,
    size_link: rbtree::Link,
}

impl ReservationNode {
    fn split(&self, num_pages_to_take: usize) -> IntrusiveRef<ReservationNode> {
        IntrusiveRef::from_box(Box::new(ReservationNode {
            num_pages: Cell::new(self.num_pages.get() - num_pages_to_take),
            start_addr: Cell::new(self.start_addr.get() + num_pages_to_take * 4096),
            address_link: Default::default(),
            size_link: Default::default(),
        }))
    }
}

impl ReservationNode {
    fn end(&self) -> usize {
        self.start_addr.get() + self.num_pages.get() * 4096
    }
}

intrusive_adaptor!(SizeOrder = ReservationNode { size_link: rbtree::Link });
intrusive_adaptor!(AddressOrder = ReservationNode { address_link: rbtree::Link });

impl<'a> rbtree::TreeAdaptor<'a> for SizeOrder {
    type Key = usize;
    fn get_key(&self, container: &'a ReservationNode) -> usize {
        container.num_pages.get()
    }
}

impl<'a> rbtree::TreeAdaptor<'a> for AddressOrder {
    type Key = usize;
    fn get_key(&self, container: &'a ReservationNode) -> usize {
        container.start_addr.get()
    }
}

pub struct Hier {
    free_blocks: RefCell<RBTree<SizeOrder>>,
    all_blocks: RefCell<RBTree<AddressOrder>>,
    top_level: *mut LevelNode,
}

impl ::core::fmt::Debug for Hier {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        f.write_str("some Hier")
    }
}

impl Hier {
    pub fn new<A: ::AllocatorBundle>(alloc: &A) -> Hier {
        let nl = new_level(alloc, 0);
        Hier::from_toplevel(unsafe { transmute(nl) })
    }

    pub fn from_toplevel(top_level: *mut LevelNode) -> Hier {
        Hier {
            free_blocks: RefCell::new(Default::default()),
            all_blocks: RefCell::new(Default::default()),
            top_level: top_level,
        }
    }

    /// "Mock" mapping pages into the VSpace, updating internal bookkeeping but not modifying the
    /// paging structures at all.
    ///
    /// This is useful during bootstrap or to otherwise record actions on the vspace that did not
    /// occur via this manager.
    pub fn mock_map_pages_at_vaddr<A: ::AllocatorBundle>(&self,
                                    caps: &[seL4_CPtr],
                                    vaddr: usize,
                                    rights: seL4_CapRights,
                                    size_bits: u8,
                                    attrs: seL4_ARCH_VMAttributes,
                                    alloc: &A) {
        unsafe { transmute::<_, &LevelNode>(self.top_level) }
            .map_pages_at_vaddr(caps, vaddr, 0, rights, size_bits, attrs, alloc, true);
    }

    /// "Mock" unmapping pages into the VSpace, updating internal bookkeeping but not modifying the
    /// paging structures at all.
    ///
    /// This is useful during bootstrap or to otherwise record actions on the vspace that did not
    /// occur via this manager.
    pub fn mock_unwrap(&self, vaddr: usize, bytes: usize, caps: Option<&mut Vec<seL4_CPtr>>) {
        self.unmap_raw(vaddr, bytes, caps, true)
    }

    pub fn mock_change_protection<A: ::AllocatorBundle>(&self, 
                                                        vaddr: usize,
                                                        bytes: usize,
                                                        rights: seL4_CapRights,
                                                        attrs: seL4_ARCH_VMAttributes,
                                                        alloc: &A) {
        unsafe { transmute::<_, &LevelNode>(self.top_level) }
            .change_protection(vaddr, bytes, rights, attrs, alloc, true);
    }

    fn unmap_raw(&self, vaddr: usize, bytes: usize, mut caps: Option<&mut Vec<seL4_CPtr>>, mock: bool) {
        let bytes = bytes + bytes % 4096;
        unsafe { &*self.top_level }.walk_table_range(vaddr, vaddr + bytes, |_, _, _, level_node| {
            match unsafe { *level_node } {
                LevelEntry::Page { cap, .. } => {
                    if !mock {
                        unsafe { UNMAP_PAGE(cap) };
                    }
                    match caps {
                        Some(ref mut v) => v.push(cap),
                        None => {}
                    }
                    unsafe { *level_node = LevelEntry::Free; }
                }
                _ => {}
            }
            None::<()>
        });
    }

}

pub struct Reservation {
    reservation: IntrusiveRef<ReservationNode>,
}

impl Reservation {
    fn res(&self) -> &ReservationNode {
        &*self.reservation
    }
}

impl ::VSpaceReservation for Reservation {
    fn start_vaddr(&self) -> usize {
        self.res().start_addr.get()
    }
    fn end_vaddr(&self) -> usize {
        // FIXME: page size shouldn't be constant?
        self.res().end()
    }
}

impl Hier {
    fn reserve_inner(&self, num_pages: usize, block: &mut ReservationNode) -> Reservation {
        let mut free = self.free_blocks.borrow_mut();
        let leftover = block.split(num_pages);
        block.num_pages.set(num_pages);
        let new = unsafe { free.cursor_mut_from_ptr(&*block).remove().unwrap() };
        free.insert(leftover);
        self.all_blocks.borrow_mut().insert(leftover);
        Reservation { reservation: new }
    }

    fn coalesce(&self, block: &ReservationNode) {
        let mut all = self.all_blocks.borrow_mut();
        let mut free = self.free_blocks.borrow_mut();
        {
            // remove it from the tree, we'll be doing exciting things with the block and the sort
            // orders.
            unsafe {
                all.cursor_mut_from_ptr(block).remove();
            }
            if block.size_link.is_linked() {
                unsafe {
                    free.cursor_mut_from_ptr(block).remove();
                }
            }
        }
        // coalesce forward, absorbing free ranges by adding their num_pages to our own.
        let num_pages = unsafe {
            let mut fwd = all.cursor_mut_from_ptr(block);
            let mut num_pages = block.num_pages.get();
            fwd.move_next();
            while !fwd.is_null() && fwd.get().unwrap().size_link.is_linked() {
                let val = fwd.remove().unwrap().into_box();
                num_pages += val.num_pages.get();
                free.cursor_mut_from_ptr(&*val).remove();
                let _ = drop(val);
            }
            num_pages
        };
        // coalesce backward, absorbing free ranges by extending our start_addr.
        let start_addr = unsafe {
            let mut bkd = all.cursor_mut_from_ptr(block);
            let mut start_addr = block.start_addr.get();
            bkd.move_prev();
            while !bkd.is_null() && bkd.get().unwrap().size_link.is_linked() {
                let val = bkd.remove().unwrap().into_box();
                start_addr = val.start_addr.get();
                free.cursor_mut_from_ptr(&*val).remove();
                let _ = drop(val);
                bkd.move_prev();
            }
            start_addr
        };
        block.num_pages.set(num_pages);
        block.start_addr.set(start_addr);
        unsafe {
            all.insert(IntrusiveRef::from_raw(block as *const _ as *mut _));
            free.insert(IntrusiveRef::from_raw(block as *const _ as *mut _));
        }
    }
}

impl ::VSpaceManager for Hier {
    type Reservation = Reservation;

    fn map_at_vaddr<A: ::AllocatorBundle>(&self,
                                          caps: &[seL4_CPtr],
                                          vaddr: usize,
                                          size_bits: u8,
                                          res: &Self::Reservation,
                                          rights: seL4_CapRights,
                                          attrs: seL4_ARCH_VMAttributes,
                                          alloc: &A)
                                          -> bool {
        if vaddr < res.res().start_addr.get() || vaddr > res.res().end() {
            return false;
        }

        unsafe { transmute::<_, &LevelNode>(self.top_level) }
            .map_pages_at_vaddr(caps, vaddr, 0, rights, size_bits, attrs, alloc, false);
        true
    }

    fn change_protection<A: ::AllocatorBundle>(&self,
                                               vaddr: usize,
                                               bytes: usize,
                                               rights: seL4_CapRights,
                                               attrs: seL4_ARCH_VMAttributes,
                                               alloc: &A) {
        unsafe { transmute::<_, &LevelNode>(self.top_level) }
            .change_protection(vaddr, bytes, rights, attrs, alloc, false);
    }

    fn unmap(&self, vaddr: usize, bytes: usize, caps: Option<&mut Vec<seL4_CPtr>>) {
        self.unmap_raw(vaddr, bytes, caps, false)
    }

    fn reserve<A: ::AllocatorBundle>(&self, bytes: usize, _: &A) -> Option<Self::Reservation> {
        let bytes = bytes + bytes % 4096;
        let num_pages = bytes / 4096;
        let mut free = self.free_blocks.borrow_mut();
        let mut best_fit = free.lower_bound_mut(Bound::Included(&num_pages));
        match unsafe { best_fit.get_mut() } {
            Some(block) => Some(self.reserve_inner(num_pages, block)),
            None => return None,
        }
    }

    fn reserve_at_vaddr<A: ::AllocatorBundle>(&self,
                                              vaddr: usize,
                                              bytes: usize,
                                              _: &A)
                                              -> Option<Self::Reservation> {
        let bytes = bytes + bytes % 4096;
        let vaddr = 4096 * (vaddr / 4096);
        let mut all = self.all_blocks.borrow_mut();
        // see if the requested range is free.
        let mut containing = all.upper_bound_mut(Bound::Included(&vaddr));
        if containing.get().unwrap().size_link.is_linked() {
            return None;
        }
        containing.move_next();
        match containing.get() {
            Some(c) => {
                if c.start_addr.get() <= vaddr + bytes {
                    return None;
                }
            }
            None => {}
        }
        containing.move_prev();
        Some(self.reserve_inner(bytes / 4096, unsafe { containing.get_mut().unwrap() }))
    }

    fn unreserve<A: ::AllocatorBundle>(&self, reservation: Self::Reservation, _: &A) {
        self.coalesce(&*reservation.reservation);
    }

    fn unreserve_at_vaddr<A: ::AllocatorBundle>(&self, vaddr: usize, _: &A) -> Result<(), ()> {
        let vaddr = 4096 * (vaddr / 4096);
        let mut all = self.all_blocks.borrow_mut();
        let (ptr, is_free) = {
            let containing = all.upper_bound_mut(Bound::Included(&vaddr));
            let ptr = containing.get_raw().unwrap();
            let is_free = containing.get().unwrap().size_link.is_linked();
            (ptr, is_free)
        };
        if !is_free {
            unsafe {
                self.coalesce(transmute::<*const ReservationNode, &ReservationNode>(ptr));
            }
            return Ok(());
        }
        return Err(());
    }

    fn get_cap(&self, vaddr: usize) -> Option<seL4_CPtr> {
        match unsafe { (&*self.top_level).get_level_entry(vaddr) } {
            Some(&LevelEntry::Page { cap, .. }) => Some(cap),
            _ => None,
        }
    }

    fn root(&self) -> seL4_CPtr {
        unsafe { (*self.top_level).table_cap }
    }

    fn minimum_slots(&self) -> usize {
        // conservative - might need a handful of caps when creating deepl
        32
    }
    fn minimum_untyped(&self) -> usize {
        // conservative - 16KiB per hierarchy level is more than enough for storing paging
        // structures.
        HIER_LEVELS.len() * (16 * 1024)
    }
    fn minimum_vspace(&self) -> usize {
        // conservative - 16KiB per hierarchy level is more than enough for storing LevelNodes.
        HIER_LEVELS.len() * (16 * 1024)
    }
}
