// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use sel4::{Window, CNodeInfo};
use sel4_sys::seL4_CPtr;
use core::cell::{RefCell, Cell};
use core::mem::size_of;
use collections::Vec;
use bitmap::{Bitmap, OneBit};

/// A "bitmap allocator" for slots in a window of a CNode.
///
/// This tracks precisely which slots have been allocated using a single bit per slot.
#[derive(Debug)]
pub struct BitmapAllocator {
    window: Window,
    info: CNodeInfo,
    bitmap: RefCell<Bitmap<Vec<usize>, OneBit>>,
    remaining: Cell<usize>,
}

impl BitmapAllocator {
    /// Create a new `BitmapAllocator` for `window` encoded with `info`.
    ///
    /// Can panic if allocation of the underlying bitmap panics. In the future, may return `None`
    /// in that case. Currently will never return `None`.
    pub fn new(window: Window, info: CNodeInfo) -> Option<BitmapAllocator> {
        let bm = Bitmap::from_storage(window.num_slots,
                                      (),
                                      vec![!0usize; window.num_slots / size_of::<usize>()*8])
                     .unwrap();

        Some(BitmapAllocator {
            window: window,
            info: info,
            bitmap: RefCell::new(bm),
            remaining: Cell::new(window.num_slots),
        })
    }

    /// The window managed by this allocator.
    pub fn window(&self) -> &Window {
        &self.window
    }

    pub fn slots_remaining(&self) -> usize {
        self.remaining.get()
    }
}

impl ::CSpaceManager for BitmapAllocator {
    fn allocate_slot<A: ::AllocatorBundle>(&self, _: &A) -> Result<seL4_CPtr, ()> {
        let mut bm = self.bitmap.borrow_mut();
        let idx = match bm.first_set() {
            Some(idx) => idx,
            None => return Err(()),
        };
        bm.set(idx, 0);
        self.remaining.set(self.remaining.get() - 1);
        Ok(self.window
               .cptr_to(&self.info, idx)
               .expect("allocated out of bounds of window, should not happen"))
    }

    fn free_slot<A: ::AllocatorBundle>(&self, cptr: seL4_CPtr, _: &A) -> Result<(), ()> {
        let mut bm = self.bitmap.borrow_mut();
        let radix = self.info.decode(cptr).radix as usize;
        let idx = radix - self.window.first_slot_idx;

        match bm.get(idx) {
            Some(0) => panic!("Double free of slot {:?} in {:?}", idx, self),
            Some(_) => (),
            None => panic!("Free of out-of-bounds slot {:?} in {:?}", idx, self),
        }

        bm.set(idx, 1);
        self.remaining.set(self.remaining.get() + 1);
        Ok(())
    }

    fn slot_info(&self, _: seL4_CPtr) -> Option<&CNodeInfo> {
        Some(&self.info)
    }

    fn slot_window(&self, cptr: seL4_CPtr) -> Option<Window> {
        let radix = self.info.decode(cptr).radix as usize;
        Some(Window {
            first_slot_idx: radix,
            num_slots: 1,
            ..self.window
        })
    }

    // this manager uses a constant amount of resources.

    fn minimum_slots(&self) -> usize {
        0
    }
    fn minimum_untyped(&self) -> usize {
        0
    }
    fn minimum_vspace(&self) -> usize {
        0
    }
}
